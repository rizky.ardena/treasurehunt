package main;

import java.util.List;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

public class TreasureHuntMain {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		char[][] map = TheGrid();
		System.out.println("The Grid Map");
		GenerateLayout(map);
		System.out.println("\nThe Possible Treasure Spots");
		FindTreasure(map);
		//FindTreasure2(map); //same complexity with the worst
	}

	private static void GenerateLayout(char[][] map) {
		for(int i=map.length-1; i>=0; i--) {
			for(int j=0;j<map[i].length;j++) {
				System.out.print(map[i][j]);
			}
			System.out.println("");
		}
	}
	
	private static char[][] TheGrid(){
		char[][] grid = {{'#','#','#','#','#','#','#','#'},
						{'#','X','#','.','.','.','.','#'},
						{'#','.','.','.','#','.','#','#'},
						{'#','.','#','#','#','.','.','#'},
						{'#','.','.','.','.','.','.','#'},
						{'#','#','#','#','#','#','#','#'}};
		return grid;
	}
	
	//Worst case search O(n^3),
	private static void FindTreasure(char[][] map) {
		char[][] treasureMap = map;
		int x = 1;
		int y = 1;
		while(treasureMap[x][y]!='#') {
			x++;
			int j = y+1;
			while(treasureMap[x][j]!='#') {
				int i = x-1;
				while(treasureMap[i][j]!='#') {
					treasureMap[i][j] = '$';
					i--;
				}
				j++;
			}
		}
		GenerateLayout(treasureMap);
	}
	
	private static ArrayList<Integer> FindClearPathVerticalUp(char[][] map, int row, int col) {
		ArrayList<Integer> results = new ArrayList<Integer>();
		int i = row+1;
		int j = col;
		while(map[i][j] != '#') {
			results.add(i);
			i++;
		}
		return results;
	}

	private static ArrayList<Integer> FindClearPathVerticalDown(char[][] map, int row, int col) {
		ArrayList<Integer> results = new ArrayList<Integer>();
		int i = row-1;
		int j = col;
		while(map[i][j] != '#') {
			results.add(i);
			i--;
		}
		return results;
	}

	private static ArrayList<Integer> FindClearPathHorizontalRight(char[][] map, int row, int col) {
		ArrayList<Integer> results = new ArrayList<Integer>();
		int i = row;
		int j = col+1;
		while(map[i][j] != '#') {
			results.add(j);
			j++;
		}
		return results;
	}

	private static void FindTreasure2(char[][] map) {
		int xStartingPoint = 1;
		int yStartingPoint = 1;
		ArrayList<Integer> firstSteps = FindClearPathVerticalUp(map,xStartingPoint,yStartingPoint);
		HashMap<Integer,ArrayList<Integer>> secondSteps = new HashMap<Integer,ArrayList<Integer>>();
		
		Iterator<Integer> first = firstSteps.iterator();
		while(first.hasNext()) {
			int row = first.next();
			secondSteps.put(row, FindClearPathHorizontalRight(map,row,yStartingPoint));
		}
		
		HashMap<Integer,Integer> treasureSpots = new HashMap<Integer,Integer>();
		for (int i : secondSteps.keySet()) {
		      System.out.println("row: " + i + " value: " + secondSteps.get(i).toString());
		}
		
	}
}
